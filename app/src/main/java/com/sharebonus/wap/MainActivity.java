package com.sharebonus.wap;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.sharebonus.wap.javascript.JavaScriptInterface;
import com.sharebonus.wap.utils.PermissionUtil;


public class MainActivity extends AppCompatActivity {


    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayout debugLinearLayout;
    private WebView mWebView;
    private EditText editText;
    private Button button;

    private JavaScriptInterface mJavaScript;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Window window = getWindow();
        window.setStatusBarColor(ResourcesCompat.getColor(getResources(),R.color.colorStatusBar,null));

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        debugLinearLayout = (LinearLayout) findViewById(R.id.debugLinearLayout);
        mWebView = (WebView) findViewById(R.id.webView);
        editText = (EditText) findViewById(R.id.editText);
        button = (Button) findViewById(R.id.button);

        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccessFromFileURLs(true);

        mJavaScript = new JavaScriptInterface(this);

        mWebView.setWebViewClient(new WebViewClient());
        mWebView.addJavascriptInterface(mJavaScript, Constants.JCA_NAME);
        mWebView.setWebChromeClient(webChromeClient);
        mWebView.setWebViewClient(webViewClient);
        mWebView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        mWebView.requestFocus();

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mWebView.loadUrl(mWebView.getUrl());
            }
        });

        mSwipeRefreshLayout.setOnChildScrollUpCallback(new SwipeRefreshLayout.OnChildScrollUpCallback() {
            @Override
            public boolean canChildScrollUp(SwipeRefreshLayout parent, @Nullable View child) {
                return mWebView.getScrollY() > 0;
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_light,
                android.R.color.holo_red_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_green_light);


//        if (DEBUG.debug) {
//            getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//
//            debugLinearLayout.setVisibility(View.VISIBLE);
//            editText.setText(Constants.URL);
//
//            button.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    loadUrl(editText.getText().toString().trim());
//                }
//            });
//
//            loadUrl(editText.getText().toString().trim());
//        } else {
            loadUrl(Constants.URL);
//        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.ACTION_WECHAT_USER_INFO);
        filter.addAction(Constants.ACTION_WECHAT_PAY);
        filter.addAction(Constants.ACTION_ALI_PAY);
        registerReceiver(mJavaScript.mReceiver, filter);
    }


    @Override
    protected void onDestroy() {
        unregisterReceiver(mJavaScript.mReceiver);
        super.onDestroy();
    }

    private void loadUrl(String url) {
        mWebView.loadUrl(url);
    }

    private WebChromeClient webChromeClient = new WebChromeClient() {
        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            return super.onJsAlert(view, url, message, result);
        }


        @Override
        public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
            return super.onJsConfirm(view, url, message, result);
        }


        @Override
        public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {
            return super.onJsPrompt(view, url, message, defaultValue, result);
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            if (newProgress == 100) {
                mSwipeRefreshLayout.setRefreshing(false);
            } else if (!mSwipeRefreshLayout.isRefreshing()) {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        }

    };

    private WebViewClient webViewClient = new WebViewClient() {

        public void onPageFinished(WebView view, String url) {
            mJavaScript.setNowUrl(url);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    };

    public WebView getWebView() {
        return mWebView;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PermissionUtil.PERMISSIONS_READ_EXTERNAL_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mJavaScript.share();
                } else {
                    Toast.makeText(this, getString(R.string.permission_denial), Toast.LENGTH_SHORT).show();
                }
                break;

            case PermissionUtil.PERMISSIONS_WRITE_EXTERNAL_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mJavaScript.share();
                } else {
                    Toast.makeText(this, getString(R.string.permission_denial), Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    public static final int RESULT_SHARE_IMAGE = 201;
    public static final int RESULT_QRCODE = 0x0000c0de;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        DEBUG.d("onActivityResult", String.valueOf(requestCode));
        switch (requestCode) {
            case RESULT_SHARE_IMAGE:
                mJavaScript.callWebViewJavascript("shareCallback(1)");
                break;

            case RESULT_QRCODE:
                IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
                if (scanningResult != null) {
                    if (scanningResult.getContents() != null) {
                        String scanContent = scanningResult.getContents();

                        DEBUG.d("scanContent.toString()", scanContent);

                        if (scanContent.length() > 3 && "http".equals(scanContent.substring(0, 4))) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(scanContent)));
                        } else {
                            mJavaScript.callWebViewJavascript("qrcodeCallback('" + scanContent + "')");
                        }
                    }
                    return;
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.error), Toast.LENGTH_LONG).show();
                }
                break;

            default:
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
