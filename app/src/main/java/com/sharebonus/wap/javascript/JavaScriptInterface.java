package com.sharebonus.wap.javascript;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore.Images.Media;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.sharebonus.wap.Constants;
import com.sharebonus.wap.DEBUG;
import com.sharebonus.wap.MainActivity;
import com.sharebonus.wap.alipay.Alipay;
import com.sharebonus.wap.qrcode.QRCodeActivity;
import com.sharebonus.wap.R;
import com.sharebonus.wap.utils.BitmapUtil;
import com.sharebonus.wap.utils.PermissionUtil;
import com.sharebonus.wap.wxapi.WeChat;
import com.tencent.mm.opensdk.modelpay.PayReq;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by lun on 2018/3/21.
 */

public class JavaScriptInterface {


    private MainActivity mActivity;
    private ClipboardManager mClipboardManager;
    private WebView mWebView;

    private String loginCallbackMethod;
    private String payWeChatCallbackMethod;
    private String payAlipayCallbackMethod;

    private String nowUrl = "";
    private String shareType;
    private String shareText;
    private String shareImageUrl;
    private String shareUrl;
    private String shareTitle;

    private WeChat mWechat;
    private Alipay mAlipay;

    public JavaScriptInterface(MainActivity activity) {
        this.mActivity = activity;
        this.mWebView = activity.getWebView();
        this.mClipboardManager = (ClipboardManager) mActivity.getSystemService(Context.CLIPBOARD_SERVICE);
        this.mWechat = new WeChat(mActivity);
        this.mAlipay = new Alipay(mActivity);

        if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M && !PermissionUtil.checkPermissionREAD_PHONE_STATE(mActivity)) {
            return;
        }
    }

    // 判斷APP內外
    @JavascriptInterface
    public boolean is_kingkr_obj() {
        return true;
    }

    //複製文字
    @JavascriptInterface
    public String copyText(String text) {
        assert mClipboardManager != null;
        mClipboardManager.setPrimaryClip(ClipData.newPlainText(null, text));
        if (mClipboardManager.hasPrimaryClip()) {
            mClipboardManager.getPrimaryClip().getItemAt(0).getText();
        }
//        showMessage(mActivity.getString(R.string.copy_ok));
          return mActivity.getString(R.string.copy_ok);
    }

    // 複製當前網址
    @JavascriptInterface
    public String copyUrlToClipboard() {
        assert mClipboardManager != null;
        mClipboardManager.setPrimaryClip(ClipData.newPlainText(null, nowUrl));
        if (mClipboardManager.hasPrimaryClip()) {
            mClipboardManager.getPrimaryClip().getItemAt(0).getText();
        }
        return nowUrl + "\n" + mActivity.getString(R.string.copy_ok);
    }

    // 第三方登入
    @JavascriptInterface
    public void login(String type, String parameter, String callbackMethod) {
        if ("WEIXIN".equals(type)) {
            loginCallbackMethod = callbackMethod;
            mWechat.login();
        }
    }

    // 调起支付
    @JavascriptInterface
    public void payType(String parameter, String forwardurl, String callbackMethod) {

        switch (forwardurl) {
            case "WEIXIN":
                payWeChatCallbackMethod = callbackMethod;
                mWechat.payType(parameter);
                break;

            case "ALIPAY":
                payAlipayCallbackMethod = callbackMethod;
                //mAlipay.pay("測試", "測試2", "0.1");
                mAlipay.pay(parameter);
                break;

            default:
                break;
        }

    }

    // QR CODE
    @JavascriptInterface
    public void qrcode(int type) {
        IntentIntegrator scanIntegrator = new IntentIntegrator(mActivity);
        scanIntegrator.setCaptureActivity(QRCodeActivity.class);
        scanIntegrator.setPrompt(mActivity.getString(R.string.qrcode_center));
        scanIntegrator.setTimeout(300000);
        scanIntegrator.setBeepEnabled(true);
        scanIntegrator.initiateScan();
//        mActivity.startActivityForResult(scanIntegrator.createScanIntent(), MainActivity.RESULT_QRCODE);
    }

    //APP内跳转浏览器開網頁
    @JavascriptInterface
    public void awakeOtherBrowser(String url) {
        mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
    }

    //微信分享圖文
    @JavascriptInterface
    public void share(String type, String text, String imageUrl, String url, String title) {
        this.shareType = type;
        this.shareText = text;
        this.shareImageUrl = imageUrl;
        this.shareUrl = url;
        this.shareTitle = title;

        DEBUG.d("shareType", type);
        DEBUG.d("shareText", text);
        DEBUG.d("shareImageUrl", imageUrl);
        DEBUG.d("shareUrl", url);
        DEBUG.d("shareTitle", title);

        share();
    }

    @JavascriptInterface
    public void share(String type) {
        this.shareType = type;
        share();
    }

    @JavascriptInterface
    public void share(String type, String imageUrl) {
        this.shareType = type;
        this.shareImageUrl = imageUrl;
        share();
    }

    public void setNowUrl(String url) {
        DEBUG.d("setNowUrl", url);
        nowUrl = url;
    }


    public void share() {

        Bitmap bitmap = null;
        Intent intent = new Intent();

        switch (shareType) {
            case "share":
                if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M && (!PermissionUtil.checkPermissionREAD_EXTERNAL_STORAGE(mActivity) || !PermissionUtil.checkPermissionWRITE_EXTERNAL_STORAGE(mActivity))) {
                    return;
                }
                bitmap = BitmapUtil.getIntentImage(shareImageUrl);
                if (bitmap == null) {
                    callWebViewJavascript("shareCallback(0)");
                } else {
                    mWechat.share(shareText, bitmap, shareUrl, shareTitle);
                }
                return;

            case "screenshot":
                if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M && (!PermissionUtil.checkPermissionREAD_EXTERNAL_STORAGE(mActivity) || !PermissionUtil.checkPermissionWRITE_EXTERNAL_STORAGE(mActivity))) {
                    return;
                }
                bitmap = BitmapUtil.getScreenImage(mActivity);
                intent.setAction(Intent.ACTION_SEND);
                break;

            case "image":
                if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M && (!PermissionUtil.checkPermissionREAD_EXTERNAL_STORAGE(mActivity) || !PermissionUtil.checkPermissionWRITE_EXTERNAL_STORAGE(mActivity))) {
                    return;
                }
                bitmap = BitmapUtil.getIntentImage(shareImageUrl);
                intent.setAction(Intent.ACTION_SEND);
                break;

            default:
                return;
        }

        if (bitmap == null) {
            callWebViewJavascript("shareCallback(0)");
        } else {
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(Media.insertImage(mActivity.getContentResolver(), bitmap, "title", null)));
            mActivity.startActivityForResult(Intent.createChooser(intent, mActivity.getString(R.string.share_to)), MainActivity.RESULT_SHARE_IMAGE);
            bitmap.recycle();
        }
    }


    public void callWebViewJavascript(final String methodString) {
        mWebView.post(new Runnable() {
            @Override
            public void run() {
                mWebView.loadUrl("javascript:" + methodString);
            }
        });
    }


//    private void showMessage(String text) {
//        AlertDialog alertDialog = new AlertDialog.Builder(mActivity).create();
//        alertDialog.setMessage(text);
//        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, mActivity.getString(R.string.ok),
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                });
//        alertDialog.show();
//    }

    public BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context mContext, Intent intent) {
            DEBUG.d("mReceiver action", intent.getAction());
            switch (intent.getAction()) {
                case Constants.ACTION_WECHAT_USER_INFO:
                    DEBUG.d("mReceiver ACTION_WECHAT_USER_INFO", intent.getStringExtra(Constants.ACTION_WECHAT_USER_INFO));
                    callWebViewJavascript(loginCallbackMethod + "('" + intent.getStringExtra(Constants.ACTION_WECHAT_USER_INFO) + "')");
                    break;

                case Constants.ACTION_WECHAT_PAY:
                    DEBUG.d("mReceiver ACTION_WECHAT_PAY", intent.getStringExtra(Constants.ACTION_WECHAT_PAY));
                    callWebViewJavascript(payWeChatCallbackMethod + "('" + intent.getIntExtra(Constants.ACTION_WECHAT_PAY,0) + "')");
                    break;

                case Constants.ACTION_ALI_PAY:
                    DEBUG.d("mReceiver ACTION_ALI_PAY", intent.getStringExtra(Constants.ACTION_ALI_PAY));
                    callWebViewJavascript(payAlipayCallbackMethod + "('" + intent.getStringExtra(Constants.ACTION_ALI_PAY) + "')");
                    break;

                default:
                    break;
            }
        }
    };

}