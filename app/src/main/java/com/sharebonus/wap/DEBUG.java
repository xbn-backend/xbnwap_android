package com.sharebonus.wap;

import android.util.Log;

public class DEBUG {

    public static final boolean debug = true;

    public static void d(String tag, String text) {
        if (debug) {
            Log.d(tag, text);
        }
    }
}
