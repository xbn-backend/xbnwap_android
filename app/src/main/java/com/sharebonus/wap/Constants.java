package com.sharebonus.wap;

public class Constants {

    // CN
    public static final String URL = "http://r.xuebona.com.cn/to.php";
    public static final String WECHAT_PAY_URL = "https://www.xuebona.com/Get_deposit/wxpay_send";

    //TW
    // public static final String URL = "http://www.sharebonus.com.tw/to.php";
    // public static final String WECHAT_PAY_URL = "https://www.sharebonus.tw/Get_deposit/wxpay_send";

    // UAT
//    public static final String URL = "https://www.xuebona.com/waptest.html";

    // develop
//    public static final String URL = "file:///android_asset/waptest.html";

    public static final String JCA_NAME = "app";

    public static final String ACTION_WECHAT_USER_INFO = "ACTION_WECHAT_USER_INFO";
    public static final String ACTION_WECHAT_PAY = "ACTION_WECHAT_PAY";
    public static final String ACTION_ALI_PAY = "ACTION_ALI_PAY";

    public static final String WECHAT_APP_ID = "wxf87d7a03b4f40b32";
    public static final String WECHAT_SECRET = "43aec9cee57dbb773df5ddd06b6fc35b";
    // public static final String WECHAT_PAY_URL = "https://www.xuebona.com/pay/weixin/WxAppPay/get.php";


}
