package com.sharebonus.wap;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.AsyncTask;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;



public class WelcomeActivity extends AppCompatActivity {


    private ImageView welcomeImageView;
    private ViewPager viewPager;
    private LinearLayout potLinearLayout;
    private Button goButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        Window window = getWindow();
        window.setStatusBarColor(ResourcesCompat.getColor(getResources(),R.color.colorStatusBar,null));

        welcomeImageView = (ImageView) findViewById(R.id.welcomeImageView);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        potLinearLayout = (LinearLayout) this.findViewById(R.id.potLinearLayout);
        goButton = (Button) this.findViewById(R.id.goButton);

        startWelcome();

    }


    private void startWelcome() {
        welcomeImageView.setVisibility(View.VISIBLE);
        SharedPreferences sharedPreferences = getSharedPreferences("sharebonus", 0);
        if (sharedPreferences.getBoolean("first", true)) {
            sharedPreferences.edit().putBoolean("first", false).commit();
            mHandler.sendEmptyMessageDelayed(FRIST, 3000);
        } else {
            //mHandler.sendEmptyMessageDelayed(FRIST, 3000);
            mHandler.sendEmptyMessageDelayed(MAIN, 3000);
        }
    }

    private void showViewPage() {
        welcomeImageView.setVisibility(View.GONE);
        viewPager.setVisibility(View.VISIBLE);
        potLinearLayout.setVisibility(View.VISIBLE);

        int nowIndex = 0;
        int[] imageRids = {R.drawable.intro01, R.drawable.intro02, R.drawable.intro03, R.drawable.intro04};
        final ImageView[] potImageViews = new ImageView[imageRids.length];


        android.view.ViewGroup.LayoutParams layoutParams = new android.view.ViewGroup.LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, android.view.ViewGroup.LayoutParams.MATCH_PARENT);
        final List<ImageView> imageViewList = new ArrayList<ImageView>();

        for (int i = 0; i < imageRids.length; i++) {
            ImageView imageView = new ImageView(this);
            imageView.setImageResource(imageRids[i]);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setLayoutParams(layoutParams);
            imageViewList.add(imageView);

            potImageViews[i] = new ImageView(this);
            potImageViews[i].setImageResource(R.drawable.ic_pot_1);
            potLinearLayout.addView(potImageViews[i]);
        }
        imageViewList.add(new ImageView(this));

        WelcomeViewPagerAdapter adapter = new WelcomeViewPagerAdapter(imageViewList);

        potImageViews[nowIndex].setImageResource(R.drawable.ic_pot_2);

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
//                goButton.setVisibility((position == potImageViews.length - 1) ? View.VISIBLE : View.GONE);
                if (position >= potImageViews.length) {
                    mHandler.sendEmptyMessageDelayed(MAIN, 0);
                } else {
                    setIndex(position);
                    new ScrollTask(position).execute();
                }
            }

            public void setIndex(int position) {
                for (int i = 0; i < potLinearLayout.getChildCount(); i++) {
                    potImageViews[i] = (ImageView) potLinearLayout.getChildAt(i);
                    potImageViews[i].setImageResource(R.drawable.ic_pot_1);
                }
                potImageViews[position].setImageResource(R.drawable.ic_pot_2);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        new ScrollTask(0).execute();

//        goButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mHandler.sendEmptyMessageDelayed(MAIN, 0);
//            }
//        });
    }


    private static final int MAIN = 0;
    private static final int FRIST = 1;
    private Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {

            switch (msg.what) {
                case MAIN:
                    startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
                    overridePendingTransition(R.anim.slide_right_in, R.anim.slide_left_out);
                    finish();
                    break;

                case FRIST:
                    showViewPage();
                    break;

                default:
                    break;
            }
        }

    };


    class ScrollTask extends AsyncTask<Void, Void, Void> {

        int pageIdx;

        public ScrollTask(int pageIdx) {
            this.pageIdx = pageIdx;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            if(viewPager.getCurrentItem()==pageIdx) {
                pageIdx++;
                viewPager.setCurrentItem(pageIdx);
            }
        }
    }

}
