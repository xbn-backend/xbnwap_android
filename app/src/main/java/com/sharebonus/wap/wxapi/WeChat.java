package com.sharebonus.wap.wxapi;

import android.app.Activity;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sharebonus.wap.Constants;
import com.sharebonus.wap.DEBUG;
import com.sharebonus.wap.MainActivity;
import com.sharebonus.wap.R;
import com.sharebonus.wap.utils.BitmapUtil;
import com.sharebonus.wap.utils.JsonUtil;
import com.tencent.mm.opensdk.constants.Build;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.modelmsg.WXImageObject;
import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.mm.opensdk.modelmsg.WXTextObject;
import com.tencent.mm.opensdk.modelmsg.WXWebpageObject;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by lun on 2018/3/23.
 */

public class WeChat {


    private IWXAPI api;
    private Context mContext;

    public WeChat(Context context) {
        this.mContext = context;
        api = WXAPIFactory.createWXAPI(mContext, Constants.WECHAT_APP_ID, true);
        api.registerApp(Constants.WECHAT_APP_ID);
    }


    public void login() {
        if (!api.isWXAppInstalled()) {
            showNoWechat();
            return;
        }

        SendAuth.Req req = new SendAuth.Req();
        req.scope = "snsapi_userinfo";
        req.state = "sharebonus_wx_login" + String.valueOf(System.currentTimeMillis());
        api.sendReq(req);

    }

    public void payType(String parameter) {
            if(parameter==null) {
                DEBUG.d("Null Input", "Null Parameter !!");
                return;
            }
            boolean isWeixinShareable = api.isWXAppInstalled() && api.isWXAppSupportAPI();
            DEBUG.d("isWeixinShareable", String.valueOf(isWeixinShareable));
            if (!isWeixinShareable) {
                showNoWechat();
                return;
            }

            boolean isPaySupported = api.getWXAppSupportAPI() >= Build.PAY_SUPPORTED_SDK_INT;
            DEBUG.d("isPaySupported", String.valueOf(isPaySupported));
            if (!isPaySupported) {
                showNoWechatPaySupported();
                return;
            }

            String json = parameter.trim();
            WechatOrderData result = new Gson().fromJson(json, WechatOrderData.class);
            try {
                PayReq req = new PayReq();
                req.appId = result.getAppid();
                req.partnerId = result.getPartnerid();
                req.prepayId = result.getPrepayid();
                req.packageValue = "Sign=WXPay";
                req.nonceStr = result.getNoncestr();
                req.timeStamp = result.getTimestamp();
                req.sign = result.getSign();
                api.sendReq(req);
            } catch (Exception e) {
                e.printStackTrace();
            }
            //new payTypeTask().execute();
    }


    public void share(String shareText, Bitmap bitmap, String shareUrl, String shareTitle) {


        boolean isWeixinShareable = api.isWXAppInstalled() && api.isWXAppSupportAPI();
        if (!isWeixinShareable) {
            showNoWechat();
            return;
        }

        WXWebpageObject webObject = new WXWebpageObject();
        webObject.webpageUrl = shareUrl;

        WXImageObject imageObject = new WXImageObject(bitmap);

        Bitmap thumbBitmap = Bitmap.createScaledBitmap(bitmap, 150, 150, true);
        bitmap.recycle();

        WXMediaMessage msg = new WXMediaMessage();
        msg.mediaObject = webObject;
        msg.thumbData = BitmapUtil.Bitmap2Bytes(thumbBitmap);
        msg.description = shareText;
        msg.title = shareTitle;

        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = String.valueOf(System.currentTimeMillis());
        req.message = msg;
        req.scene = SendMessageToWX.Req.WXSceneSession;

        api.sendReq(req);


    }


    private String buildTransaction(String type) {
        return (type == null) ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
    }

    private void showNoWechat() {
        Toast.makeText(mContext, mContext.getString(R.string.no_wechat), Toast.LENGTH_SHORT).show();
    }

    private void showNoWechatPaySupported() {
        Toast.makeText(mContext, mContext.getString(R.string.no_wechat_pay_supported), Toast.LENGTH_SHORT).show();
    }


    private class payTypeTask extends AsyncTask<WechatOrderData, Integer, WechatOrderData> {

        private ProgressDialog mProgressDialog;


        @Override
        protected void onPreExecute() {
            mProgressDialog = new ProgressDialog(mContext);
            mProgressDialog.setMessage(mContext.getString(R.string.progressing));
            mProgressDialog.show();
        }


        @Override
        protected WechatOrderData doInBackground(WechatOrderData... params) {
            WechatOrderData order = null;
            try {
                String json = JsonUtil.getInternetJSON(Constants.WECHAT_PAY_URL);
                json = json.substring(json.indexOf("{"));

                order = new Gson().fromJson(json, WechatOrderData.class);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return order;
        }


        @Override
        protected void onPostExecute(WechatOrderData result) {
            mProgressDialog.dismiss();

            // String timeStamp = String.valueOf(System.currentTimeMillis());
            // String partnerId = "1481098472";
            // String partnerId = "1486339502";
            // String sign = result.getSign();

            DEBUG.d("微信支付", "appId:" + result.getAppid());
            DEBUG.d("微信支付", "partnerId:" + result.getPartnerid());
            DEBUG.d("微信支付", "prepayId:" + result.getPrepayid());
            DEBUG.d("微信支付", "packageValue:" + "Sign=WXPay");
            DEBUG.d("微信支付", "nonceStr:" + result.getNoncestr());
            DEBUG.d("微信支付", "timeStamp:" + result.getTimestamp());
            DEBUG.d("微信支付", "sign:" + result.getSign());

            try {
                PayReq req = new PayReq();
                req.appId = result.getAppid();
                req.partnerId = result.getPartnerid();
                req.prepayId = result.getPrepayid();
                req.packageValue = "Sign=WXPay";
                req.nonceStr = result.getNoncestr();
                req.timeStamp = result.getTimestamp();
                req.sign = result.getSign();
                api.sendReq(req);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        protected void onCancelled() {
            mProgressDialog.dismiss();
        }
    }


}
