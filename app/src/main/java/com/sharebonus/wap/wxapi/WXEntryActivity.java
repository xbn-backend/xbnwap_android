package com.sharebonus.wap.wxapi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;
import com.google.gson.Gson;
import com.sharebonus.wap.Constants;
import com.sharebonus.wap.DEBUG;
import com.sharebonus.wap.MainActivity;
import com.sharebonus.wap.R;
import com.sharebonus.wap.utils.JsonUtil;
import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WXEntryActivity extends AppCompatActivity implements IWXAPIEventHandler {


    private IWXAPI mWxApi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mWxApi = WXAPIFactory.createWXAPI(this, Constants.WECHAT_APP_ID, false);
        mWxApi.registerApp(Constants.WECHAT_APP_ID);
        mWxApi.handleIntent(getIntent(), this);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        mWxApi.handleIntent(intent, this);
    }

    @Override
    public void onResp(BaseResp baseResp) {

        DEBUG.d("onResp", "errCode：" + baseResp.errCode);
        DEBUG.d("onResp", "errStr：" + baseResp.errStr);

        switch (baseResp.errCode) {
            case BaseResp.ErrCode.ERR_OK:

                switch (baseResp.getType()) {
                    case ConstantsAPI.COMMAND_SENDAUTH:
                        String code = ((SendAuth.Resp) baseResp).code;
                        DEBUG.d("登入 baseResp.getType()", "code = " + code);
//                        Toast.makeText(this, getString(R.string.login_ok), Toast.LENGTH_SHORT).show();
                        new getInfoTask().execute(code);
                        break;

                    case ConstantsAPI.COMMAND_SENDMESSAGE_TO_WX:
                        DEBUG.d("分享", "ok");
                        Toast.makeText(this, getString(R.string.share_ok), Toast.LENGTH_SHORT).show();
                        break;

                    case ConstantsAPI.COMMAND_PAY_BY_WX:

                        DEBUG.d("COMMAND_PAY_BY_WX", "onPayFinish,errCode=" + baseResp.errCode);

                        Intent intent = new Intent(Constants.ACTION_WECHAT_PAY);
                        intent.putExtra(Constants.ACTION_WECHAT_PAY, baseResp.errCode);
                        sendBroadcast(intent);
                        break;

                    default:
                        break;

                }


                break;

            case BaseResp.ErrCode.ERR_USER_CANCEL:
                Toast.makeText(this, getString(R.string.cancel), Toast.LENGTH_SHORT).show();
                break;

            case BaseResp.ErrCode.ERR_AUTH_DENIED:
                Toast.makeText(this, getString(R.string.denied), Toast.LENGTH_SHORT).show();
                break;

            default:
                Toast.makeText(this, getString(R.string.back), Toast.LENGTH_SHORT).show();
                break;
        }


        finish();
    }

    @Override
    public void onReq(BaseReq baseReq) {

        DEBUG.d("onReq", "BaseReq：" + baseReq.getType());
        DEBUG.d("onReq", "BaseReq：" + baseReq.toString());


//        try {
//            Intent intent = new Intent(this, MainActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    private class getInfoTask extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... params) {
            JSONObject userInfoJsonObject = null;
            try {
                String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + Constants.WECHAT_APP_ID
                        + "&secret=" + Constants.WECHAT_SECRET
                        + "&code=" + params[0]
                        + "&grant_type=authorization_code";
                JSONObject jsonObject = new JSONObject(JsonUtil.getInternetJSON(url));

                String openid = jsonObject.optString("openid");
                String access_token = jsonObject.optString("access_token");
                DEBUG.d("openid", openid);
                DEBUG.d("access_token", access_token);

                String userInfoUrl = "https://api.weixin.qq.com/sns/userinfo?access_token=" + access_token
                        + "&openid=" + openid;

                return JsonUtil.getInternetJSON(userInfoUrl);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        }

        @Override
        protected void onPostExecute(String userInfoJson) {
            Intent intent = new Intent(Constants.ACTION_WECHAT_USER_INFO);
            intent.putExtra(Constants.ACTION_WECHAT_USER_INFO, userInfoJson);
            sendBroadcast(intent);
        }
    }
}
