package com.sharebonus.wap.alipay;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;
import com.sharebonus.wap.Constants;
import com.sharebonus.wap.DEBUG;
import com.sharebonus.wap.R;
import com.sharebonus.wap.utils.SignUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

public class Alipay {

    public static final String APPID = "2017071807796832";
    public static final String RSA2_PRIVATE = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCfYTtd29qTn7bxOFVSK0Yv8KeOi3gdah4+SGQC34MZ3vE48mgkRvt257u6pZcxf1vxvBOCPRwqLvuS0QHIR/Wmhcn5LnlQCnK1knK7KkcjytQurZOZH2Y74iiZPS5deN7XyK5/M0zG+y7fo+okil1KX5pT27dJ5E127GoE1tF65NvuBatHCo7j+mjvkjMMFWVV6gY5lrjRKTThW8s7C/UQfBKqzu+csdgyrCkWsTjPk8h5SWFF3lhoZowZICSegBpsoR/UZPV73SzZpZfv+TLyzuM4AgM7Y79qunEpXXV4x6rqGPfi40ffQGQ13TF1R/sNTXv5D+GOnlOvd/zfMEpZAgMBAAECggEABwr05KDid8OHAgHLQ2ZGI7gur03acPJPg9EPGlFmKa6AWlPb5j58vjRfR1Ap7jYCihwVgJvSssqAs2wo+AC2CjW/RxOWqMLNbtiZ2Ut0+U/tMGLbMtkAW4US5jKd6eN9dWuYymYp7mELITbNEfBLTw4Kc8oxv0InainoRW54s0V311mBCKccCESMHRQEXUCYDd8NngpJ+kmunWWTR8LoDIvQTXRJMMTbCGkMP6vkMGJwyXOWYblOW5DD5/s/laSaTl8W+CrP7hayBggU3eaSBoBOisrjozgTcCquOXud0cGsxFVegDDX/xkDWpNNXTpYRPst1AvVJLhh3KlaRBY/QQKBgQDOWspGQg8fptBev4g+Ob21sqplFAaIncML+E674Ax0+y5ujpT1leM8/90xVTCtW+FieKZAKEEstplfg7IHZGDCZhiD0y4rYGBjytR/HgT2vcoaKshlVXytJ+Wzy6KlppEnwwV0y35qVRGcofewlDlgDPOan6lHaGU9GM/qUMYcqwKBgQDFuU0KTzlAnrRCnlkIgrMmlHoEaMD7dvcNaWNGwEkQZZSjSn6xcnize/6k87Me4ART+pfURT5mqs6inf215k4Fur3iFNuN6tA5FP7Ovo5oT696R8tvZCj1PsEUsE6ssbotwneucfimYn3EbojeLipoJNHaWIv6RvjcJ1hBdOktCwKBgAqSwFaFP/0hMQAJ1FNL5hqPQwfW1NbfLoVIfdBGlJzR2fk0ORG0UmZnvc+vS8U2NgAzqmETLvO5j3ZDazXRwNt1G8B1a6IxV/8E/4pb4Wk7JEXe91Gp5a+BodVqk7TlKf7dkUQAwUN1hGeALQ7RKXdwcnLzx1qvULCmutCIvv3tAoGBAKyFHVp0e9ENaN2Dys2pNOXhwYFdYZ1IVRIHtY7DMj0zVPkRg/ASoIyDEVIeLVkwP7aCvb2CWYs9WCT906TGead4JDxC1LtEekCEODxuelU7yjHYuQHNlAtAoToLB+qY/Ij22HSGj12mLHSbuaKbB3dtQQnSHvjWukdlsRUUDGy3AoGAConEg5puWHV3Q2zbZvEj99PV0kECOCe0KwPH1dtgR2WRcn1J8RYJmemP9SeDihN1TRGnj2oPd9qdiPlpsIXUeHUqLsuW2CzdLDpb4I3xjux+14aDkJhbx4T4ObZy9Rjpk0hSNzChZ0+Qfu5kRLjPYcDy3szZBqgJ+BS/nX0aEYE=";

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static final int PAY = 1;
    private Activity mActivity;


    public Alipay(Activity activity) {
        this.mActivity = activity;
    }


    public void pay(final String param) {
        try {
            String jsonStr = getArrValue(param.split("&"),"biz_content");
            jsonStr = URLDecoder.decode(jsonStr,"UTF-8");
//            DEBUG.d("jsonStr=",jsonStr );
            JSONObject json = new JSONObject(jsonStr.trim());
            String name = json.getString("subject");
            String desc = json.getString("body");
            String price = json.getString("total_amount");
            pay(name,desc,price);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }


    public void pay(final String name, final String text, final String price) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("app_id", APPID);
                params.put("biz_content", "{\"timeout_express\":\"30m\",\"product_code\":\"QUICK_MSECURITY_PAY\",\"total_amount\":\"" + price + "\",\"subject\":\"" + name + "\",\"body\":\"" + text + "\",\"out_trade_no\":\"" + getOutTradeNo() + "\"}");
                params.put("charset", "utf-8");
                params.put("method", "alipay.trade.app.pay");
                params.put("sign_type", "RSA2");
                //params.put("timestamp", "2018-04-12 16:55:53");
                params.put("timestamp", sdf.format(new Date()));
                params.put("version", "1.0");

                String sign = getSign(params);
                sign = buildOrderParam(params) + "&" + sign;
                DEBUG.d("sign", sign);

                Map<String, String> result = new PayTask(mActivity).payV2(sign, true);
                DEBUG.d("msp", result.toString());

                Message msg = new Message();
                msg.what = PAY;
                msg.obj = result;
                mHandler.sendMessage(msg);
            }
        }).start();
    }

    private String getSign(Map<String, String> map) {
        List<String> keys = new ArrayList<String>(map.keySet());

        Collections.sort(keys);

        StringBuilder authInfo = new StringBuilder();
        for (int i = 0; i < keys.size() - 1; i++) {
            String key = keys.get(i);
            String value = map.get(key);
            authInfo.append(buildKeyValue(key, value, false));
            authInfo.append("&");
        }

        String tailKey = keys.get(keys.size() - 1);
        String tailValue = map.get(tailKey);
        authInfo.append(buildKeyValue(tailKey, tailValue, false));

        String oriSign = SignUtils.sign(authInfo.toString(), RSA2_PRIVATE, true);
        String encodedSign = "";

        try {
            encodedSign = URLEncoder.encode(oriSign, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "sign=" + encodedSign;
    }


    private String getOutTradeNo() {
        SimpleDateFormat format = new SimpleDateFormat("MMddHHmmss", Locale.getDefault());
        Date date = new Date();
        String key = format.format(date);

        Random r = new Random();
        key = key + r.nextInt();
        key = key.substring(0, 15);
        return key;
    }

    private static String buildKeyValue(String key, String value, boolean isEncode) {
        StringBuilder sb = new StringBuilder();
        sb.append(key);
        sb.append("=");
        if (isEncode) {
            try {
                sb.append(URLEncoder.encode(value, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                sb.append(value);
            }
        } else {
            sb.append(value);
        }
        return sb.toString();
    }

    public static String buildOrderParam(Map<String, String> map) {
        List<String> keys = new ArrayList<String>(map.keySet());

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < keys.size() - 1; i++) {
            String key = keys.get(i);
            String value = map.get(key);
            sb.append(buildKeyValue(key, value, true));
            sb.append("&");
        }

        String tailKey = keys.get(keys.size() - 1);
        String tailValue = map.get(tailKey);
        sb.append(buildKeyValue(tailKey, tailValue, true));

        return sb.toString();
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            DEBUG.d("handleMessage msg.what", String.valueOf(msg.what));
            switch (msg.what) {
                case PAY: {
                    @SuppressWarnings("unchecked")
                    PayResult payResult = new PayResult((Map<String, String>) msg.obj);

                    String resultInfo = payResult.getResult();
                    DEBUG.d("resultInfo", resultInfo);

                    String resultStatus = payResult.getResultStatus();
                    DEBUG.d("resultStatus", resultStatus);

                    Intent intent = new Intent(Constants.ACTION_ALI_PAY);
                    intent.putExtra(Constants.ACTION_ALI_PAY, resultStatus);
                    mActivity.sendBroadcast(intent);

//                    if (TextUtils.equals(resultStatus, "9000")) {
//                        Toast.makeText(mActivity, mActivity.getString(R.string.complete), Toast.LENGTH_SHORT).show();
//                    } else {
//                        Toast.makeText(mActivity, mActivity.getString(R.string.cancel), Toast.LENGTH_SHORT).show();
//                    }
                    break;
                }

                default:
                    break;
            }
        }

        ;
    };


    private String getArrValue(String arr[] , String key) {
        for(int i=0 ;i<arr.length; i++) {
            if(arr[i].startsWith(key))
                return arr[i].substring(arr[i].indexOf("=")+1) ;
        }
        return "";
    }

}