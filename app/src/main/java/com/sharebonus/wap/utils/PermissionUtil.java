package com.sharebonus.wap.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.sharebonus.wap.MainActivity;
import com.sharebonus.wap.R;

public class PermissionUtil {


    public static final int PERMISSIONS_READ_EXTERNAL_STORAGE = 101;
    public static final int PERMISSIONS_WRITE_EXTERNAL_STORAGE = 102;
    public static final int PERMISSIONS_READ_PHONE_STATE = 103;

    public static boolean checkPermissionREAD_PHONE_STATE(Activity activity) {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_PHONE_STATE)) {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSIONS_READ_PHONE_STATE);
            } else {
                Toast.makeText(activity, activity.getString(R.string.permission_denial), Toast.LENGTH_SHORT).show();
            }
            return false;
        }
        return true;
    }

    public static boolean checkPermissionREAD_EXTERNAL_STORAGE(Activity activity) {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSIONS_READ_EXTERNAL_STORAGE);
            } else {
                Toast.makeText(activity, activity.getString(R.string.permission_denial), Toast.LENGTH_SHORT).show();
            }
            return false;
        }
        return true;
    }

    public static boolean checkPermissionWRITE_EXTERNAL_STORAGE(Activity activity) {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSIONS_WRITE_EXTERNAL_STORAGE);
            } else {
                Toast.makeText(activity, activity.getString(R.string.permission_denial), Toast.LENGTH_SHORT).show();
            }
            return false;
        }
        return true;
    }


}
