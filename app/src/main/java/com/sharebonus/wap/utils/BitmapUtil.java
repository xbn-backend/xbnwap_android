package com.sharebonus.wap.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.view.View;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by lun on 2018/3/27.
 */

public class BitmapUtil {

    public static byte[] Bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }

    public static Bitmap getScreenImage(Activity activity) {
        try {
            View mView = activity.getWindow().getDecorView();
            mView.setDrawingCacheEnabled(true);
            mView.buildDrawingCache();
            Bitmap mFullBitmap = mView.getDrawingCache();

            Rect mRect = new Rect();
            activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(mRect);
            int mStatusBarHeight = mRect.top;

            int mPhoneWidth = activity.getWindowManager().getDefaultDisplay().getWidth();
            int mPhoneHeight = activity.getWindowManager().getDefaultDisplay().getHeight();

            Bitmap mBitmap = Bitmap.createBitmap(mFullBitmap, 0, mStatusBarHeight, mPhoneWidth, mPhoneHeight - mStatusBarHeight);
            mView.destroyDrawingCache();
            mFullBitmap.recycle();

            return mBitmap;
        } catch (Exception e) {
            return null;
        }
    }

    public static Bitmap getIntentImage(String url) {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setDoInput(true);
            connection.connect();
            return BitmapFactory.decodeStream(connection.getInputStream());
        } catch (Exception e) {
            return null;
        }
    }
}
