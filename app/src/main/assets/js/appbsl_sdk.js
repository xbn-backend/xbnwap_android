function is_kingkr_obj(){
return android.isInApp();
}

function copyText(text){
 android.copyText(text);
}

function copyUrlToClipboard(){
 return android.copyUrlToClipboard();
}

function login(type, parameter, result){
  android.login(type, parameter, result);
}

function payType(parameter,method,result){
 return android.payType(parameter,method,result);
}

function qrcode(type){
 return android.qrcode(type);
}

function awakeOtherBrowser(url){
 return android.awakeOtherBrowser(url);
}


function share() {
    if(arguments.length==1){
        android.share(arguments[0]);
    }else if(arguments.length==2){
        android.share(arguments[0],arguments[1]);
    }else if(arguments.length==5){
        android.share(arguments[0],arguments[1],arguments[2],arguments[3],arguments[4]);
    }
}


function shareCallback(res){
        alert(res);
}

function qrcodeCallback(result){
　　　　alert(result);
}